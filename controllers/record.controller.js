const db = require("../models");
const Record = db.records;
const User = db.users;
const Op = db.Sequelize.Op;

// Create and Save a new Record
exports.create = (req, res) => {
   // Validate request
    if (Object.keys(req.body).length !== 1 ) {
    res.status(400).send({
      message: "Record content should be {'KEY': 'VALUE'}"
    });
    return;
  }

  // Create a Record
  const record = {
    key: Object.keys(req.body)[0],
    value: req.body[Object.keys(req.body)[0]],
  };

  //Save record in the database
  Record.create(record)
    .then(data => {
      _timestamp = Math.floor((new Date()).getTime(data.createdAt) / 1000)
      let obj = {key: data.key, value: data.value, timestamp: _timestamp}
      res.send(obj);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial."
      });
    });
};


// Find the Record with a key and close timestamp 
exports.findByKeyAndTime = (req, res) => {
    //Validate request
    if (!req.params.key) {
      res.status(400).send({
        message: "Key of a record can not be empty!"
     });
      return;
    }

    const _key = req.params.key;
    const _timestamp = req.query.timestamp 
    var condition = _timestamp ? {[Op.and]: [{ key: _key}, {createdAt: {[Op.lte]: new Date(_timestamp * 1000)}}]} : {key: _key};
   
    Record.findAll({
         where: condition
        ,order: [
          ['createdAt', 'DESC']
        ],
        limit: 1
    }).then(data => {
        let val = {value: data[0].value};
        // res.send(data);
        res.send(val);
    }).catch(err => {
        res.status(500).send({
          message: "Error retrieving Record with key= " + _key 
        });
    });
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
  
};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
  
};
