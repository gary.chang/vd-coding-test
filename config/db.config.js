module.exports = {
    HOST: "postgres",
    PORT: "5432",
    USER: "postgres",
    PASSWORD: "password",
    DB: "node_db",
    dialect: "postgres",
    pool: {
      max: 30,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };