const express = require("express");
const cors = require("cors");
const swaggerUi = require('swagger-ui-express');
const swaggerFile = require('./swagger_output.json');


const app = express();

var corsOptions = {
  origin: "http://localhost:8080"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));

// checks what is the current state of the table in the database 
// , and then performs the necessary changes in the table to make it match the model.
const db = require("./models");
db.sequelize.sync({ alter: true }).then(() => {
    console.log("Perform database sync.");
  }
);

require("./routes/record.route.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = app;
