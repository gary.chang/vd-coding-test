module.exports = app => {
    const records = require("../controllers/record.controller.js");
    

    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", records.create);
  
    // Retrieve a single Tutorial with id
    router.get("/:key?", records.findByKeyAndTime);
  
    // Update a Record with id
    // router.put("/:id", records.update);
  
  
    app.use('/object', router);
  };